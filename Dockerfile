FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y software-properties-common \
    && apt-add-repository -y -u ppa:ansible/ansible \
    && apt-get install -y \
      curl \
      git \
      ansible \
      build-essential \
    && apt-get clean autoclean \
    && apt-get autoremove --yes

COPY . .
COPY vault-password .

RUN ansible-playbook init.yml --vault-password-file ./vault-password

